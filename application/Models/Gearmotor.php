<?php

namespace App\Models;

class Gearmotor
{

	const ID = 1;

	public function searchCount($params, &$data, $typeId = null)
	{
		if ($typeId == null) {
			$query = "SELECT COUNT(*) AS count FROM gearmotors AS gm, equipments AS eq, groups AS gr, types AS t
 						WHERE eq.id = :id_equipment AND eq.id = gr.id_equipment AND gr.id = t.id_group AND t.id = gm.id_type ";
			$data['id_equipment'] = $params['id_equipment'];

			if (isset($params['id_group'])) {
				$query .= " AND gr.id = :id_group ";
				$data['id_group'] = $params['id_group'];
			}
			if (isset($params['id_type'])) {
				$query .= " AND t.id = :id_type ";
				$data['id_type'] = $params['id_type'];
			}

			$query .= $this->buildSearchQuery($params, $data);
			return $query;
		} else {
			$query = "SELECT COUNT(*) AS count FROM gearmotors AS gm WHERE gm.id_type = :id_type ";
			$data['id_type'] = $typeId;
			$query .= $this->buildSearchQuery($params, $data);
			return $query;
		}
	}

	public function searchItems($params, &$data, $typeId = null)
	{
		if ($typeId == null) {
			$query = "SELECT gm.id, gm.output_shaft_speed, gm.torque, gm.service_factor, gm.gear_ratio, gm.name,
						eq.name AS eq_name, gr.name AS gr_name, t.full_name AS t_name
						FROM gearmotors AS gm, equipments AS eq, groups AS gr, types AS t
						WHERE eq.id = :id_equipment AND eq.id = gr.id_equipment AND gr.id = t.id_group AND t.id = gm.id_type ";
			$data['id_equipment'] = $params['id_equipment'];

			if (isset($params['id_group'])) {
				$query .= " AND gr.id = :id_group ";
				$data['id_group'] = $params['id_group'];
			}
			if (isset($params['id_type'])) {
				$query .= " AND t.id = :id_type ";
				$data['id_type'] = $params['id_type'];
			}
			$query .= $this->buildSearchQuery($params, $data);
			return $query;
		} else {
			$query = "SELECT gm.id, gm.output_shaft_speed, gm.torque, gm.service_factor, gm.gear_ratio, gm.name
							FROM gearmotors AS gm WHERE gm.id_type = :id_type ";
			$data['id_type'] = $typeId;
			$query .= $this->buildSearchQuery($params, $data);
			return $query;
		}
	}

	private function buildSearchQuery($params, &$data)
	{
		$query = "";

		/** Output Shaft Speed */
		if (isset($params['speed_shaft_min'])) {
			$query .= " AND gm.output_shaft_speed >= :speed_shaft_min ";
			$data['speed_shaft_min'] = $params['speed_shaft_min'];
		}
		if (isset($params['speed_shaft_max'])) {
			$query .= " AND gm.output_shaft_speed <= :speed_shaft_max ";
			$data['speed_shaft_max'] = $params['speed_shaft_max'];
		}

		/** Torque */
		if (isset($params['torque_min'])) {
			$query .= " AND gm.torque >= :torque_min ";
			$data['torque_min'] = $params['torque_min'];
		}
		if (isset($params['torque_max'])) {
			$query .= " AND gm.torque <= :torque_max ";
			$data['torque_max'] = $params['torque_max'];
		}

		/** Service Factor */
		if (isset($params['service_factor_min'])) {
			$query .= " AND gm.service_factor >= :service_factor_min ";
			$data['service_factor_min'] = $params['service_factor_min'];
		}
		if (isset($params['service_factor_max'])) {
			$query .= " AND gm.service_factor <= :service_factor_max ";
			$data['service_factor_max'] = $params['service_factor_max'];
		}

		/** Gear Ratio */
		if (isset($params['gear_ratio_min'])) {
			$query .= " AND gm.gear_ratio >= :gear_ratio_min ";
			$data['gear_ratio_min'] = $params['gear_ratio_min'];
		}
		if (isset($params['gear_ratio_max'])) {
			$query .= " AND gm.gear_ratio <= :gear_ratio_max ";
			$data['gear_ratio_max'] = $params['gear_ratio_max'];
		}

		/** Selects */
		if (isset($params['motor_power'])) {
			$query .= " AND gm.motor_power = :motor_power ";
			$data['motor_power'] = $params['motor_power'];
		}
		if (isset($params['number_poles'])) {
			$query .= " AND gm.number_poles = :number_poles ";
			$data['number_poles'] = $params['number_poles'];
		}

		return $query;
	}
}