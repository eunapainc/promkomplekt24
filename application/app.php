<?php

namespace App;

use App\Cache\Cacher;
use App\Routing\Dispatcher;
use App\Routing\RouteCollector;
use App\Utils\SlugUtil;

class App
{

	public function init()
	{
		$httpMethod = $_SERVER['REQUEST_METHOD'];

		$uri = $url =  $_SERVER["REQUEST_URI"];

		$cacher = new Cacher();
		if ($uri != "/" && $cacher->exists($uri)) {
//			echo $cacher->load($uri);
//			return;
		}
		$uri = strtok($_SERVER["REQUEST_URI"], '?');

		include_once __DIR__ . "/Routing/bootstrap.php";

		/** @var Dispatcher $dispatcher */
		$dispatcher = \App\Routing\simpleDispatcher(function (RouteCollector $routeCollector) {
			$routeCollector->addRoute('GET', '/', 'EquipmentController@index');
			$routeCollector->addRoute('GET', '/search', 'SearchController@index');
			$routeCollector->addRoute('POST', '/search/count', 'SearchController@showCount');
			$routeCollector->addRoute('GET', '/search/items', 'SearchController@getItems');

			$routeCollector->addRoute('GET', '/equip/{id}', 'GroupController@index');
			$routeCollector->addRoute('GET', '/group/{id}', 'TypeController@index');
			$routeCollector->addRoute('GET', '/type/{id}', 'ListCardController@index');
			$routeCollector->addRoute('GET', '/gearmotors/{id}', 'GearMotorsController@index');
		});

		if ($uri == "/" || preg_match('/search/', $uri)) {
			$properUrl = $uri;
		} else {
			$properUrl = SlugUtil::loadUrl($uri);
		}

		if ($properUrl == null) {
			echo " 404";
			return;
		}

		$route = $dispatcher->dispatch($httpMethod, $properUrl);
		switch ($route[0]) {
			case Dispatcher::NOT_FOUND:
				echo "Not found";
				break;
			case Dispatcher::METHOD_NOT_ALLOWED:
				echo "Method not allowed";
				break;
			case Dispatcher::FOUND:
				ob_start();

				$handler = explode('@', $route[1]);
				$controller = "App\\Controllers\\" . $handler[0];
				$controller = new $controller;

				$method = $handler[1];
				$controller->$method($route[2]);

				$content = ob_get_contents();
				$cacher->save($url, $content);
				ob_end_clean();
				echo $content;
				break;
		}

	}
}