<?php

namespace App\Database;

use Exception;
use PDO;
use PDOException;

class Database
{
	/** @var  PDO */
	private static $pdo;
	private static $connected = false;

	public static function connect()
	{
		if (static::$connected) {
			return;
		}
		try {
			$dsn = "mysql:host=localhost;dbname=movedb";
			$pdo = new PDO($dsn, 'root', '');
			$pdo->exec("SET CHARACTER SET utf8");
			$pdo->exec("SET NAMES 'utf8'");
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			static::$pdo = $pdo;
			static::$connected = true;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public static function preparedFetch($query, $data, $fetchMode = PDO::FETCH_OBJ)
	{
		$pdoStmt = static::preparedExecute($query, $data);
		$pdoStmt->setFetchMode($fetchMode);
		return $pdoStmt->fetch();
	}

	public static function preparedFetchAll($query, $data)
	{
		$pdoStmt = static::preparedExecute($query, $data);
		return $pdoStmt->fetchAll();
	}

	public static function preparedExecute($query, $data)
	{
		$pdoStmt = static::$pdo->prepare($query);
		$pdoStmt->execute($data);
		return $pdoStmt;
	}

	public static function insertWithIdReturn($query, $data)
	{
		$pdoStmt = static::$pdo->prepare($query);
		$pdoStmt->execute($data);
		return static::$pdo->lastInsertId();
	}

	public static function loadSingleValue($column, $table, $where, $data)
	{
		$query = "select $column from $table where $where";
		$result = static::preparedFetchAll($query, $data);
		return count($result) > 0 ? $result[0][$column] : false;
	}

	public static function query($query)
	{
		$result = static::$pdo->query($query);
		if ($result) {
			return $result->fetchAll(\PDO::FETCH_ASSOC);
		} else {
			return $result;
		}
	}

	public static function execute($query)
	{
		try {
			$stmt = static::$pdo->prepare($query);
			$stmt->execute();
		} catch (Exception $e) {
			print_r(static::$pdo->errorInfo());
			echo "<br/>";
			echo "<pre>";
			print_r($e->getTrace());
			echo "</pre>";
		}
	}
}