<?php

namespace App\Controllers;

use App\Database\Database;
use App\Utils\SlugUtil;

class GroupController extends Controller
{

	public function index($params = null)
	{
		$id = $params['id'];
		Database::connect();
		$items = Database::preparedExecute("SELECT id, name, image FROM groups WHERE id_equipment = :id", [
			'id' => $id
		]);
		$items = $items->fetchAll();
		foreach ($items as &$item) {
			$url = "/group/" . $item['id'];
			$nameUrl = SlugUtil::getSlugUri() . "/" . $item['name'];
			$slug = SlugUtil::applyUrl($url, $nameUrl);
			$item['slug'] = $slug;
		}
		$this->view('catalog_group', $data = ['items' => $items]);
	}
}