<?php

namespace App\Controllers;

use App\Database\Database;

class GearMotorsController extends Controller
{

	public function index($params = null)
	{
		$id = $params['id'];
		Database::connect();
		$query = "SELECT * FROM gearmotors WHERE id = :id";
		$stmt = Database::preparedExecute($query, ['id' => $id]);
		$cardFields = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		$cards = Array();
		foreach ($cardFields[0] as $key => $value) {
			if (self::getRussianName($key)) {
				$cards[$key]['value'] = $value;
				$cards[$key]['name'] = self::getRussianName($key);
			}
		}
		$this->view('card_gear_motors', $data = [
			'card' => $cards
		]);
	}

	public static function getRussianName($name)
	{
		$array = [
			"name" => "Наименование", "output_shaft_speed" => "Скорость выходного вала, обр\\мин(n2)", "torque" => "Крутящий момент мотор-редуктора", "service_factor" => "Сервис фактор(Sf)", "gear_ratio" => "Передаточное число(i)", "motor_efficiency" => "КПД электродвигателя", "manufacturer" => "Произв", "motor_power" => "Мощность электродвигателя, kW)", "number_poles" => "Количество полюсов", "country" => "Страна производства", "unit" => "Ед изм", "unit", "price_buy" => "Цена закупачная", "weight" => "Вес", "code_custom" => "Таможеный код", "gear_efficiency" => "КПД редуктора", "axial_load_on_output_shaft" => "Осевая нагрузка вых вала, Н( 0º/90º/180º/270º)", "radial_load_on_output_shaft" => "Радиальная нагрузка вых вала, Н(0º/90º/180º/270º)", "maximum_heat_capacity" => "Максимальная тепловая емкость, kW (Pt)", "motor_speed" => "Обороты электродвигателя, об\\мин(n1)", "cos" => "cos", "current_400" => "Сила эл-го тока, А (при 400V)", "voltage" => "Напряжение, B(U)", "voltage", "frequency" => "Частота, Hz(v)", "work_mode" => "Рабочий режим", "phase" => "Фазность", "phase", "temperature_class" => "Температурный класс", "degree_of_protection" => "Степень защиты", "cooling" => "Охлаждение", "cooling", "current_230" => "Сила эл-го тока, А (при 230V)", "braking_force" => "Тормозное усилие, Н", "number_brake" => "Количество вкл и выкл тормоза в час",
		];

//		$array = [
//			"id" => "код1", "id_text" => "код", "id_type" => "Тип", "name" => "Наименование", "group" => "Группа продукции", "manufacturer" => "Произв", "country" => "Страна производства", "unit" => "Ед изм", "unit", "price_buy" => "Цена закупачная", "weight" => "Вес", "design" => "Чертеж", "price_sell" => "Продажная цена", "code_custom" => "Таможеный код", "output_shaft_speed" => "Скорость выходного вала, обр\\мин(n2)", "torque" => "Крутящий момент мотор-редуктора", "service_factor" => "Сервис фактор(Sf)", "gear_ratio" => "Передаточное число(i)", "gear_efficiency" => "КПД редуктора", "axial_load_on_output_shaft" => "Осевая нагрузка вых вала, Н( 0º/90º/180º/270º)", "radial_load_on_output_shaft" => "Радиальная нагрузка вых вала, Н(0º/90º/180º/270º)", "maximum_heat_capacity" => "Максимальная тепловая емкость, kW (Pt)", "motor_power" => "Мощность электродвигателя, kW)", "motor_speed" => "Обороты электродвигателя, об\\мин(n1)", "cos" => "cos φ", "current_400" => "Сила эл-го тока, А (при 400V)", "motor_efficiency" => "КПД электродвигателя", "voltage" => "Напряжение, B(U)", "voltage", "frequency" => "Частота, Hz(v)", "work_mode" => "Рабочий режим", "phase" => "Фазность", "phase", "temperature_class" => "Температурный класс", "degree_of_protection" => "Степень защиты", "cooling" => "Охлождение", "cooling", "current_230" => "Сила эл-го тока, А (при 230V)", "braking_force" => "Тормозное усилие, Н", "number_brake" => "Количество вкл и выкл тормоза в час", "select_flange" => "Выбрать фланец", "select_connecting_flange" => "Выбрать соединительный фланец(IEC)", "select_attitude" => "Выбрать положение в пространстве", "select_output_shaft_diameter" => "Выбрать диаметр выходного вала", "select_iar" => "Выбрать антиреверс", "select_output_shaft" => "Выбрать выходной вал", "select_torque_limiter" => "Выбрать ограничитель крутящего момента", "select_reactive_post" => "Выбрать реактивную штангу", "select_position_terminal_box" => "Выбрать положение клемной коробки", "number_poles" => "Количество полюсов"
//		];

		if (isset($array[$name])) {
			return $array[$name];
		}
		return false;
	}
}