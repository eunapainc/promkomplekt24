<?php

namespace App\Controllers;

use App\Database\Database;
use App\Utils\SlugUtil;

class EquipmentController extends Controller
{
	public function index()
	{
		Database::connect();
		$items = Database::query("SELECT id, name, image FROM equipments");
		foreach ($items as &$item) {
			$url = "/equip/" . $item['id'];
			$nameUrl = "/" . $item['name'];
			$slug = SlugUtil::applyUrl($url, $nameUrl);
			$item['slug'] = $slug;
		}
		$this->view('catalog_equip', $data = ['items' => $items]);
	}
}