<?php

namespace App\Controllers;

class Controller
{
	public function view($name, $data = [])
	{
		require PUBLIC_FOLDER . "/views/$name.php";
	}
}