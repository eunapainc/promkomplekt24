<?php

namespace App\Controllers;

use App\Database\Database;
use App\Utils\SlugUtil;

class TypeController extends Controller
{
	public function index($params = null)
	{
		$id = $params['id'];
		Database::connect();
		$items = Database::preparedExecute("SELECT id, full_name as name, image FROM types WHERE id_group = :id", [
			'id' => $id
		]);
		$items = $items->fetchAll();

		foreach ($items as &$item) {
			$url = "/type/" . $item['id'];
			$nameUrl = SlugUtil::getSlugUri() . "/" . $item['name'];
			$slug = SlugUtil::applyUrl($url, $nameUrl);
			$item['slug'] = $slug;
		}

		$this->view('catalog_type', $data = ['items' => $items]);
	}
}