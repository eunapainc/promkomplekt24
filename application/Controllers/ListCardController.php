<?php

namespace App\Controllers;

use App\Database\Database;
use App\Models\Gearmotor;
use App\Utils\SlugUtil;

class ListCardController extends Controller
{
	const ITEM_COUNT = 30;

	public function index($params)
	{
		$page = isset($_GET['page']) ? $_GET['page'] : 1;

		$id = $params['id'];
		Database::connect();

		$pagesCount = $this->getPagesCount($id);
		if ($page < 1 || $page > $pagesCount) {
			$page = 1;
		}
		$offset = ($page - 1) * self::ITEM_COUNT;
		$items = $this->getItems($id, $offset);
		$this->view('catalog_list', $data = [
			'items' => $items, 'pagesCount' => $pagesCount, 'page' => $page, 'baseUrl' => $this->getBaseUrl(), 'id_type'=>$id
		]);
	}

	private function getItems($id, $offset)
	{
		$gearmotor = new Gearmotor();
		$queryData = [];
		$query = $gearmotor->searchItems($_GET, $queryData, $id);

		$query .= " LIMIT " . self::ITEM_COUNT . " OFFSET $offset";
		$items = Database::preparedExecute($query, $queryData);
		$items = $items->fetchAll();
		foreach ($items as &$item) {
			$url = "/gearmotors/" . $item['id'];
			$nameUrl = SlugUtil::getSlugUri() . "/" . $item['name'];
			$slug = SlugUtil::applyUrl($url, $nameUrl);
			$item['slug'] = $slug;
		}
		return $items;
	}

	private function getBaseUrl()
	{
		$url = strtok($_SERVER["REQUEST_URI"], '?') . '?';
		foreach ($_GET as $key => $value) {
			if ($key != 'page') {
				$url .= "$key=$value&";
			}
		}
		return $url;
	}

	private function getPagesCount($id)
	{
		$gearmotor = new Gearmotor();
		$queryData = [];
		$query = $gearmotor->searchCount($_GET, $queryData, $id);

		$rows = Database::preparedExecute($query, $queryData);
		$rows = $rows->fetchAll();
		$count = $rows[0]['count'];
		return round($count / self::ITEM_COUNT);
	}
}