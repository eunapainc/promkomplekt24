<?php

namespace App\Controllers;

use App\Database\Database;
use App\Models\Gearmotor;
use App\Utils\SlugUtil;

class SearchController extends Controller
{

	const ITEM_COUNT = 30;

	public function index()
	{
		$data = $this->loadViewData();
		$this->view('search', $data);
	}

	public function showCount()
	{
		echo $this->getCount($_POST);
	}

	public function getItems()
	{
		$viewData = $this->loadViewData();

		$params = $_SERVER['REQUEST_METHOD'] == 'GET' ? $_GET : $_POST;
		$idEquipment = $params['id_equipment'];

		$query = "";
		$queryData = [];
		switch ($idEquipment) {
			case Gearmotor::ID:
				$gearmotor = new Gearmotor();
				$query = $gearmotor->searchItems($params, $queryData);
				break;
			default:
				break;
		}

		$page = isset($params['page']) ? $params['page'] : 1;
		$pagesCount = round($this->getCount($params) / self::ITEM_COUNT);
		if ($page < 1 || $page > $pagesCount) {
			$page = 1;
		}
		$offset = ($page - 1) * self::ITEM_COUNT;
		$query .= " LIMIT " . self::ITEM_COUNT . " OFFSET $offset";

		Database::connect();
		$items = Database::preparedExecute($query, $queryData);
		$items = $items->fetchAll(\PDO::FETCH_ASSOC);
		foreach ($items as &$item) {
			$url = "/gearmotors/" . $item['id'];
			$nameUrl = "/" . $item['eq_name'] . "/" . $item['gr_name'] . "/" . $item['t_name'] . "/" . $item['name'];
			$slug = SlugUtil::applyUrl($url, $nameUrl);
			$slug = $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['SERVER_NAME'] . $slug;
			$item['slug'] = $slug;
		}

		$viewData['items'] = $items;
		$viewData['pagesCount'] = $pagesCount;
		$viewData['page'] = $page;
		$viewData['baseUrl'] = $this->getBaseUrl();

		$this->view('search', $viewData);
	}

	private function getBaseUrl()
	{
		$url = strtok($_SERVER["REQUEST_URI"], '?') . '?';
		foreach ($_GET as $key => $value) {
			if ($key != 'page') {
				$url .= "$key=$value&";
			}
		}
		return $url;
	}

	private function loadViewData()
	{
		Database::connect();
		$equipments = Database::query("SELECT id, name FROM equipments");
		$equipments = json_encode($equipments, JSON_UNESCAPED_UNICODE);

		$groups = Database::query("SELECT g.id, g.name, e.id AS id_e FROM groups AS g, equipments AS e
									WHERE e.id = g.id_equipment");
		$groups = json_encode($groups, JSON_UNESCAPED_UNICODE);

		$types = Database::query("SELECT t.id, t.full_name AS name, g.id AS id_g FROM types AS t, groups AS g
									WHERE g.id = t.id_group");
		$types = json_encode($types, JSON_UNESCAPED_UNICODE);

		$data = [
			'equipments' => $equipments, 'groups' => $groups, 'types' => $types
		];
		return $data;
	}

	private function getCount($params)
	{
		$idEquipment = $params['id_equipment'];

		$query = "";
		$data = [];
		switch ($idEquipment) {
			case Gearmotor::ID:
				$gearmotor = new Gearmotor();
				$idType = isset($params['catalog']) ? $params['id_type'] : null;
				$query = $gearmotor->searchCount($params, $data, $idType);
				break;
			default:
				break;
		}

		Database::connect();
		$items = Database::preparedExecute($query, $data);
		$items = $items->fetchAll(\PDO::FETCH_ASSOC);

		return $items[0]['count'];
	}
}