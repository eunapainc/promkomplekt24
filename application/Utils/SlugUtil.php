<?php

namespace App\Utils;

use App\Database\Database;

class SlugUtil
{
	public static function applyUrl($url, $nameUrl)
	{
		if ($slug = self::existsUrl($url)) {
			return $slug;
		}

		$slug = self::slug($nameUrl);
		$slug = self::saveUrl($url, $slug);
		return $slug;
	}

	private static function existsSlug($slug)
	{
		Database::connect();
		$rows = Database::query("SELECT id FROM slugs WHERE slug = '$slug'");
		return $rows && count($rows) > 0;
	}

	private static function existsUrl($url)
	{
		Database::connect();
		$rows = Database::query("SELECT slug FROM slugs WHERE url = '$url'");
		if ($rows && count($rows) > 0) {
			return $rows[0]['slug'];
		}
		return false;
	}

	private static function saveUrl($url, $slug)
	{
		$exists = true;
		$index = 1;
		while ($exists) {
			$exists = self::existsSlug($slug);
			if (!$exists) {
				break;
			}
			if ($index > 1) {
				$slug = substr($slug, 0, strlen($slug - 2));
			}
			$slug .= "-$index";
		}
		$slug = preg_replace('/-{2,}/', '-', $slug);
		$slug .= '.html';

		Database::connect();
		$query = "INSERT INTO slugs(slug, url) VALUES (:slug, :url)";
		Database::preparedExecute($query, ['slug' => $slug, 'url' => $url]);
		return $slug;
	}

	public static function loadUrl($slugUri)
	{
		Database::connect();
		$query = "SELECT url FROM slugs WHERE slug = :slug";
		$stmt = Database::preparedExecute($query, ['slug' => $slugUri]);
		$rows = $stmt->fetchAll();
		if (count($rows) == 0) {
			return null;
		}
		return $rows[0]['url'];
	}

	public static function slug($url)
	{
		// переводим в транслит
		$url = self::rus2translit($url);
		// в нижний регистр
		$url = strtolower($url);
		// заменям все ненужное нам на "-"
		$url = preg_replace('~[^-a-z0-9_/]+~u', '-', $url);
		// удаляем начальные и конечные '-'
		$url = trim($url, "-");
		return $url;
	}

	private static function rus2translit($string)
	{
		$converter = array(
			'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch', 'ь' => '\'', 'ы' => 'y', 'ъ' => '\'', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya',

			'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z', 'И' => 'I', 'Й' => 'Y', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C', 'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch', 'Ь' => '\'', 'Ы' => 'Y', 'Ъ' => '\'', 'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
		);
		return strtr($string, $converter);
	}

	public static function getSlugUri()
	{
		$url = strtok($_SERVER["REQUEST_URI"], '?');
		$url = str_replace('.html', '', $url);
		return $url;
	}
}