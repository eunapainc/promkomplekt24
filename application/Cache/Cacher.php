<?php

namespace App\Cache;

class Cacher
{
	public function exists($url)
	{
		$url = $this->prepareUrl($url);
		return file_exists($this->path($url));
	}

	public function load($url)
	{
		$url = $this->prepareUrl($url);
		return file_get_contents($this->path($url));
	}

	public function save($url, $content)
	{
		$url = $this->prepareUrl($url);
		$this->createDirs($url);
		file_put_contents($this->path($url), $content);
	}

	private function createDirs($url)
	{
		$parts = explode('/', $url);
		$count = count($parts);
		if ($count > 2) {
			unset($parts[$count - 1]);
			$path = $this->path(implode('/', $parts));
			if (!file_exists($path)) {
				mkdir($path, 0777, true);
			}
		}
	}

	private function prepareUrl($url)
	{
		return str_replace('?', '_', $url);
	}

	private function path($url)
	{
		return __DIR__ . "/files$url";
	}
}