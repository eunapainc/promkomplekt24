<?php
header("Content-Type: text/html; charset=utf-8");

$dsn = "mysql:host=localhost;dbname=movedb;charset=utf8";
$pdo = new PDO($dsn, 'root', '');
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

try {
	$pdo->exec("SET CHARACTER SET utf8");
	$pdo->exec("SET NAMES 'utf8'");
	$pdo->beginTransaction();

	proceedFirstLevel($pdo);
	proceedSecondLevel($pdo);
	proceedThirdLevel($pdo);
	proceedGearmotors($pdo);

	proceedSelect1($pdo);
	proceedSelect2($pdo);
	proceedSelect3($pdo);
	proceedSelect4($pdo);
	proceedSelect5($pdo);
	proceedSelect6($pdo);
	proceedSelect7($pdo);
	proceedSelect8($pdo);
	proceedSelect9($pdo);

	proceedSklad1C($pdo);
	proceedSklad1CProduce($pdo);
	proceedSkladPoland($pdo);
	proceedSkladPolandCode($pdo);

	$pdo->commit();
} catch (Exception $e) {
	$pdo->rollBack();
	echo $e->getMessage();
}

function proceedFirstLevel($pdo)
{
	$tableName = "equipments";
	renameTable($pdo, "Промышленная комплектация", $tableName);

	renameColumn($pdo, $tableName, "код", "id", "int(11)");
	renameColumn($pdo, $tableName, "Промышленная комплектация", "name", "varchar(50)");
	renameColumn($pdo, $tableName, "фото", "image", "varchar(200)");
}

function proceedSecondLevel($pdo)
{
	$tableName = "groups";
	renameTable($pdo, "продукция группа", $tableName);

	renameColumn($pdo, $tableName, "код", "id", "int(11)");
	renameColumn($pdo, $tableName, "Код Промышленной комплектации", "id_equipment", "int(11)");
	renameColumn($pdo, $tableName, "Группа продукции", "name", "varchar(50)");
	renameColumn($pdo, $tableName, "Сокращенное имя", "short_name", "varchar(50)");
	renameColumn($pdo, $tableName, "Код ТНВЭД", "id_tnved", "varchar(50)");
	renameColumn($pdo, $tableName, "Пошлина", "tax", "int(11)");
	renameColumn($pdo, $tableName, "фото", "image", "varchar(200)");
}

function proceedThirdLevel($pdo)
{
	$tableName = "types";
	renameTable($pdo, "типы продукции", $tableName);

	renameColumn($pdo, $tableName, "код", "id", "int(11)");
	renameColumn($pdo, $tableName, "Продукция группа", "id_group", "int(11)");
	renameColumn($pdo, $tableName, "Продукция", "name", "varchar(250)");
	renameColumn($pdo, $tableName, "Полное название Типа", "full_name", "varchar(250)");
	renameColumn($pdo, $tableName, "Фото Типа", "image", "varchar(200)");
	renameColumn($pdo, $tableName, "Код ТНВЭД", "id_tnved", "varchar(250)");
	renameColumn($pdo, $tableName, "Пошлина", "tax", "int(11)");
}

function proceedGearmotors($pdo)
{
	$tableName = "gearmotors";
	renameTable($pdo, "мотор-редукторы главная", $tableName);

	renameColumn($pdo, $tableName, "код1", "id", "int(11)");
	renameColumn($pdo, $tableName, "код", "id_text", "varchar(255)");
	renameColumn($pdo, $tableName, "Тип", "id_type", "int(11)");
	renameColumn($pdo, $tableName, "Наименование", "name", "varchar(255)");
	renameColumn($pdo, $tableName, "Группа продукции", "group", "varchar(255)");

	renameColumn($pdo, $tableName, "Произв", "manufacturer", "varchar(255)");
	renameColumn($pdo, $tableName, "Страна производства", "country", "varchar(255)");

	renameColumn($pdo, $tableName, "Ед изм", "unit", "varchar(255)");
	renameColumn($pdo, $tableName, "Цена закупачная", "price_buy", "double");
	renameColumn($pdo, $tableName, "Вес", "weight", "double");
	renameColumn($pdo, $tableName, "Чертеж", "design", "varchar(255)");
	renameColumn($pdo, $tableName, "Продажная цена", "price_sell", "double");
	renameColumn($pdo, $tableName, "Таможеный код", "code_custom", "varchar(255)");

	renameColumn($pdo, $tableName, "Скорость выходного вала, обр\\мин(n2)", "output_shaft_speed", "double");
	renameColumn($pdo, $tableName, "Крутящий момент мотор-редуктора, Hm(M2)", "torque", "double");
	renameColumn($pdo, $tableName, "Сервис фактор(Sf)", "service_factor", "double");
	renameColumn($pdo, $tableName, "Передаточное число(i)", "gear_ratio", "double");
	renameColumn($pdo, $tableName, "КПД редуктора, %(КПД)", "gear_efficiency", "double");
	renameColumn($pdo, $tableName, "Осевая нагрузка вых вала, Н( 0º/90º/180º/270º)", "axial_load_on_output_shaft", "varchar(255)");
	renameColumn($pdo, $tableName, "Радиальная нагрузка вых вала, Н(0º/90º/180º/270º)", "radial_load_on_output_shaft", "varchar(255)");
	renameColumn($pdo, $tableName, "Максимальная тепловая емкость, kW (Pt)", "maximum_heat_capacity", "int(11)");
	renameColumn($pdo, $tableName, "Мощность электродвигателя, kW)", "motor_power", "double");
	renameColumn($pdo, $tableName, "Обороты электродвигателя, об\\мин(n1)", "motor_speed", "double");
	renameColumn($pdo, $tableName, "cos φ", "cos", "double");
	renameColumn($pdo, $tableName, "Сила эл-го тока, А (при 400V)", "current_400", "double");
	renameColumn($pdo, $tableName, "КПД электродвигателя", "motor_efficiency", "double");
	renameColumn($pdo, $tableName, "Напряжение, B(U)", "voltage", "varchar(255)");
	renameColumn($pdo, $tableName, "Частота, Hz(v)", "frequency", "varchar(255)");
	renameColumn($pdo, $tableName, "Рабочий режим", "work_mode", "varchar(255)");
	renameColumn($pdo, $tableName, "Фазность", "phase", "varchar(255)");
	renameColumn($pdo, $tableName, "Температурный класс", "temperature_class", "varchar(255)");
	renameColumn($pdo, $tableName, "Степень защиты", "degree_of_protection", "varchar(255)");
	renameColumn($pdo, $tableName, "Охлождение", "cooling", "varchar(255)");
	renameColumn($pdo, $tableName, "Сила эл-го тока, А (при 230V)", "current_230", "double");
	renameColumn($pdo, $tableName, "Тормозное усилие, Н", "braking_force", "int(11)");
	renameColumn($pdo, $tableName, "Количество вкл и выкл тормоза в час", "number_brake", "int(11)");

	renameColumn($pdo, $tableName, "Выбрать фланец", "select_flange", "varchar(255)");
	renameColumn($pdo, $tableName, "Выбрать соединительный фланец(IEC)", "select_connecting_flange", "varchar(255)");
	renameColumn($pdo, $tableName, "Выбрать положение в пространстве", "select_attitude", "varchar(255)");
	renameColumn($pdo, $tableName, "Выбрать диаметр выходного вала", "select_output_shaft_diameter", "varchar(255)");
	renameColumn($pdo, $tableName, "Выбрать антиреверс", "select_iar", "varchar(255)");
	renameColumn($pdo, $tableName, "Выбрать выходной вал", "select_output_shaft", "varchar(255)");
	renameColumn($pdo, $tableName, "Выбрать ограничитель крутящего момента", "select_torque_limiter", "varchar(255)");
	renameColumn($pdo, $tableName, "Выбрать реактивную штангу", "select_reactive_post", "varchar(255)");
	renameColumn($pdo, $tableName, "Выбрать положение клемной коробки", "select_position_terminal_box", "varchar(255)");
	renameColumn($pdo, $tableName, "Количество полюсов", "number_poles", "varchar(255)");
}


function proceedSelect1($pdo)
{
	$tableName = "select_1";
	renameTable($pdo, "Выборное 1", $tableName);

	renameColumn($pdo, $tableName, "Код", "id", "int(11)");
	renameColumn($pdo, $tableName, "Группа", "group", "varchar(255)");
	renameColumn($pdo, $tableName, "Свойство", "property", "varchar(255)");
	renameColumn($pdo, $tableName, "Значение", "value", "varchar(255)");
	renameColumn($pdo, $tableName, "Цена закупочная", "price_buy", "double");
	renameColumn($pdo, $tableName, "Продажная цена", "price_sell", "double");
	renameColumn($pdo, $tableName, "Вес", "weight", "double");
	renameColumn($pdo, $tableName, "Чертеж", "scheme", "varchar(255)");
	renameColumn($pdo, $tableName, "код1", "id_text", "varchar(255)");
}

function proceedSelect2($pdo)
{
	$tableName = "select_2";
	renameTable($pdo, "Выборное 2", $tableName);

	renameColumn($pdo, $tableName, "Код", "id", "int(11)");
	renameColumn($pdo, $tableName, "Группа", "group", "varchar(255)");
	renameColumn($pdo, $tableName, "Свойство", "property", "varchar(255)");
	renameColumn($pdo, $tableName, "Значение", "value", "varchar(255)");
	renameColumn($pdo, $tableName, "Цена закупочная", "price_buy", "double");
	renameColumn($pdo, $tableName, "Продажная цена", "price_sell", "double");
	renameColumn($pdo, $tableName, "Вес", "weight", "double");
	renameColumn($pdo, $tableName, "Чертеж", "scheme", "varchar(255)");
	renameColumn($pdo, $tableName, "код1", "id_text", "varchar(255)");
	renameColumn($pdo, $tableName, "Производитель", "manufacturer", "varchar(255)");
}

function proceedSelect3($pdo)
{
	$tableName = "select_3";
	renameTable($pdo, "Выборное 3", $tableName);

	renameColumn($pdo, $tableName, "Код", "id", "int(11)");
	renameColumn($pdo, $tableName, "Группа", "group", "varchar(255)");
	renameColumn($pdo, $tableName, "Свойство", "property", "varchar(255)");
	renameColumn($pdo, $tableName, "Значение", "value", "varchar(255)");
	renameColumn($pdo, $tableName, "Цена закупочная", "price_buy", "double");
	renameColumn($pdo, $tableName, "Продажная цена", "price_sell", "double");
	renameColumn($pdo, $tableName, "Вес (Количество масла)", "weight", "double");
	renameColumn($pdo, $tableName, "Чертеж", "scheme", "varchar(255)");
	renameColumn($pdo, $tableName, "код1", "id_text", "varchar(255)");
}

function proceedSelect4($pdo)
{
	$tableName = "select_4";
	renameTable($pdo, "Выборное 4", $tableName);

	renameColumn($pdo, $tableName, "Код", "id", "int(11)");
	renameColumn($pdo, $tableName, "Группа", "group", "varchar(255)");
	renameColumn($pdo, $tableName, "Свойство", "property", "varchar(255)");
	renameColumn($pdo, $tableName, "Значение", "value", "varchar(255)");
	renameColumn($pdo, $tableName, "Цена закупочная", "price_buy", "double");
	renameColumn($pdo, $tableName, "Продажная цена", "price_sell", "double");
	renameColumn($pdo, $tableName, "Вес", "weight", "double");
	renameColumn($pdo, $tableName, "Чертеж", "scheme", "varchar(255)");
	renameColumn($pdo, $tableName, "код1", "id_text", "varchar(255)");
}

function proceedSelect5($pdo)
{
	$tableName = "select_5";
	renameTable($pdo, "Выборное 5", $tableName);

	renameColumn($pdo, $tableName, "Код", "id", "int(11)");
	renameColumn($pdo, $tableName, "Группа", "group", "varchar(255)");
	renameColumn($pdo, $tableName, "Свойство", "property", "varchar(255)");
	renameColumn($pdo, $tableName, "значение", "value", "varchar(255)");
	renameColumn($pdo, $tableName, "Цена закупочная", "price_buy", "double");
	renameColumn($pdo, $tableName, "Продажная цена", "price_sell", "double");
	renameColumn($pdo, $tableName, "Вес", "weight", "double");
	renameColumn($pdo, $tableName, "Чертеж", "scheme", "varchar(255)");
	renameColumn($pdo, $tableName, "код1", "id_text", "varchar(255)");
}

function proceedSelect6($pdo)
{
	$tableName = "select_6";
	renameTable($pdo, "Выборное 6", $tableName);

	renameColumn($pdo, $tableName, "Код", "id", "int(11)");
	renameColumn($pdo, $tableName, "Группа", "group", "varchar(255)");
	renameColumn($pdo, $tableName, "Свойство", "property", "varchar(255)");
	renameColumn($pdo, $tableName, "Значение", "value", "varchar(255)");
	renameColumn($pdo, $tableName, "Цена закупочная", "price_buy", "double");
	renameColumn($pdo, $tableName, "Продажная цена", "price_sell", "double");
	renameColumn($pdo, $tableName, "Вес", "weight", "double");
	renameColumn($pdo, $tableName, "Чертеж", "scheme", "varchar(255)");
	renameColumn($pdo, $tableName, "код1", "id_text", "varchar(255)");
}

function proceedSelect7($pdo)
{
	$tableName = "select_7";
	renameTable($pdo, "Выборное 7", $tableName);

	renameColumn($pdo, $tableName, "Код", "id", "int(11)");
	renameColumn($pdo, $tableName, "Группа", "group", "varchar(255)");
	renameColumn($pdo, $tableName, "Свойство", "property", "varchar(255)");
	renameColumn($pdo, $tableName, "Значение", "value", "varchar(255)");
	renameColumn($pdo, $tableName, "Цена закупочная", "price_buy", "double");
	renameColumn($pdo, $tableName, "Продажная цена", "price_sell", "double");
	renameColumn($pdo, $tableName, "Вес", "weight", "double");
	renameColumn($pdo, $tableName, "Чертеж", "scheme", "varchar(255)");
	renameColumn($pdo, $tableName, "код1", "id_text", "varchar(255)");
}


function proceedSelect8($pdo)
{
	$tableName = "select_8";
	renameTable($pdo, "Выборное 8", $tableName);

	renameColumn($pdo, $tableName, "Код", "id", "int(11)");
	renameColumn($pdo, $tableName, "Группа", "group", "varchar(255)");
	renameColumn($pdo, $tableName, "Свойство", "property", "varchar(255)");
	renameColumn($pdo, $tableName, "Значение", "value", "varchar(255)");
	renameColumn($pdo, $tableName, "Цена закупочная", "price_buy", "double");
	renameColumn($pdo, $tableName, "Продажная цена", "price_sell", "double");
	renameColumn($pdo, $tableName, "Вес", "weight", "double");
	renameColumn($pdo, $tableName, "Чертеж", "scheme", "varchar(255)");
	renameColumn($pdo, $tableName, "код1", "id_text", "varchar(255)");
}

function proceedSelect9($pdo)
{
	$tableName = "select_9";
	renameTable($pdo, "Выборное 9", $tableName);

	renameColumn($pdo, $tableName, "Код", "id", "int(11)");
	renameColumn($pdo, $tableName, "Группа", "group", "varchar(255)");
	renameColumn($pdo, $tableName, "Свойство", "property", "varchar(255)");
	renameColumn($pdo, $tableName, "Значение", "value", "varchar(255)");
	renameColumn($pdo, $tableName, "Цена закупочная", "price_buy", "double");
	renameColumn($pdo, $tableName, "Продажная цена", "price_sell", "double");
	renameColumn($pdo, $tableName, "Вес", "weight", "double");
	renameColumn($pdo, $tableName, "Чертеж", "scheme", "varchar(255)");
	renameColumn($pdo, $tableName, "код1", "id_text", "varchar(255)");
}

function proceedSklad1C($pdo)
{
	$tableName = "storage_1C";
	renameTable($pdo, "склад 1С", $tableName);

	renameColumn($pdo, $tableName, "Код", "id", "double");
	renameColumn($pdo, $tableName, "Артикул", "article", "varchar(255)");
	renameColumn($pdo, $tableName, "ТМЦ", "tmc", "varchar(255)");
	renameColumn($pdo, $tableName, "Едизм", "unit", "varchar(255)");
	renameColumn($pdo, $tableName, "Остаток", "balance", "int(11)");
	renameColumn($pdo, $tableName, "Резерв", "reserve", "int(11)");
	renameColumn($pdo, $tableName, "Своб остаток", "free_balance", "int(11)");
}

function proceedSkladPoland($pdo)
{
	$tableName = "storage_poland";
	renameTable($pdo, "склад поляков", $tableName);

	renameColumn($pdo, $tableName, "Symbol", "symbol", "varchar(255)");
	renameColumn($pdo, $tableName, "Nazwa", "name", "varchar(255)");
	renameColumn($pdo, $tableName, "Zapas", "balance", "double");
	renameColumn($pdo, $tableName, "Rezerwacja", "reserve", "double");
}

function proceedSklad1CProduce($pdo)
{
	$tableName = "storage_1C_produce";
	renameTable($pdo, "склад 1С производство", $tableName);

	renameColumn($pdo, $tableName, "Код", "id", "double");
	renameColumn($pdo, $tableName, "Артикул", "article", "varchar(255)");
	renameColumn($pdo, $tableName, "ТМЦ", "tmc", "varchar(255)");
	renameColumn($pdo, $tableName, "Ед#изм#", "unit", "varchar(255)");
	renameColumn($pdo, $tableName, "Остаток", "balance", "int(11)");
	renameColumn($pdo, $tableName, "Резерв", "reserve", "int(11)");
	renameColumn($pdo, $tableName, "Своб# остаток", "free_balance", "int(11)");
}


function proceedSkladPolandCode($pdo)
{
	$tableName = "storage_poland_code";
	renameTable($pdo, "склад поляков код", $tableName);

	renameColumn($pdo, $tableName, "Symbol", "symbol", "varchar(255)");
	renameColumn($pdo, $tableName, "Код артикл", "id_article", "varchar(255)");
}

function renameTable(PDO $pdo, $oldName, $newName)
{
	$query = "RENAME TABLE `$oldName` TO `$newName`";
	echo $query . "<br>";
	$pdo->exec($query);
}

function renameColumn(PDO $pdo, $table, $oldName, $newName, $type)
{
	$query = "ALTER TABLE `$table` CHANGE `$oldName` `$newName` $type";
	echo "$query <br>";
	$pdo->exec($query);
}