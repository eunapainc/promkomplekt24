$(document).ready(function () {
    $.each(equipments, function (key, value) {
        addOptionInSelect("select-equipment", value);
        $('#gearmotors-search-form').hide();
    });

    $('#select-equipment').on('change', function () {
        clearSelectType('select-group', 'Выберите группу');
        clearSelectType('select-type', 'Выберите тип');
        var id_equipment = this.value;
        $.each(groups, function (key, value) {
            if (value.id_e == id_equipment) {
                addOptionInSelect("select-group", value)
            }
        });
        $('select').material_select();
        $('#gearmotors-search-form').hide();
    });

    $('#select-group').on('change', function () {
        clearSelectType('select-type', 'Выберите тип');
        var id_group = this.value;
        $.each(types, function (key, value) {
            if (value.id_g == id_group) {
                addOptionInSelect("select-type", value)
            }
        });
        $('select').material_select();
        $('#gearmotors-search-form').hide();
    });

    $('#select-type').on('change', function () {
        getCountQuery();
        $('select').material_select();
        if ($('#select-equipment').val() == 1) // проверка какую форму поиска показывать
            $('#gearmotors-search-form').show();
    });

    function clearSelectType(id, text) {
        $('#' + id)
            .find('option')
            .remove();
        $('#' + id)
            .append($("<option></option>")
                .attr("value", "")
                .attr("disabled", "")
                .attr("selected", "")
                .text(text));
    }
});


function getUrl() {
    var data = getData();
    var query;
    var isFirst = true;
    $.each(data, function (key, value) {
        if (isFirst) {
            query = "/search/items?" + key + "=" + value;
            isFirst = false;
        }
        else {
            query += "&" + key + "=" + value;
        }
    });
    return query;
}

function getData() {
    var data = getDataGearMotors();
    return data;
}

function getCountQuery() {
    var data = getData();
    $.ajax({
        type: 'POST',
        url: '/search/count',
        data: data,
        beforeSend: function () {
            showProgress()
        },
        complete: function () {
            hideProgress()
        },
        success: function (data) {
            showResult(data)
        }
    });
}

function addOptionInSelect(id, value) {
    $('#' + id)
        .append($("<option></option>")
            .attr("value", value.id)
            .text(value.name));
}

function showProgress() {
    $("#result").hide();
    $("#loader").show();
}
function hideProgress() {
    $("#loader").hide();
}

function showResult(count) {
    $("#result").show().html("Найдено: " + count + " " +
        "<a href='" + getUrl() + "'>Показать?</a>");
}