$(document).ready(function () {
    var params = getSearchParameters();
    autoFillGearmotors(params)
});


function getUrl() {
    var data = getData();
    var query;
    var isFirst = true;
    $.each(data, function (key, value) {
        if (key !== 'id_equipment' && key !== 'id_type' && key !== 'catalog') {
            if (isFirst) {
                query = "?" + key + "=" + value;
                isFirst = false;
            }
            else {
                query += "&" + key + "=" + value;
            }
        }
    });
    return query;
}

function getData() {
    var data = getDataGearMotors();
    data.catalog = 1;
    data.id_type = idType;
    return data;
}

function getCountQuery() {
    var data = getData();
    $.ajax({
        type: 'POST',
        url: '/search/count',
        data: data,
        beforeSend: function () {
            showProgress()
        },
        complete: function () {
            hideProgress()
        },
        success: function (data) {
            showResult(data)
        }
    });
}

function showProgress() {
    $("#result").hide();
    $("#loader").show();
}
function hideProgress() {
    $("#loader").hide();
}

function showResult(count) {
    $("#result").show().html("Найдено: " + count + " " +
        "<a href='" + getUrl() + "'>Показать?</a>");
}

function getSearchParameters() {
    var prmstr = window.location.search.substr(1);
    return prmstr != null && prmstr != "" ? transformToAssocArray(prmstr) : {};
}

function transformToAssocArray(prmstr) {
    var params = {};
    var prmarr = prmstr.split("&");
    for (var i = 0; i < prmarr.length; i++) {
        var tmparr = prmarr[i].split("=");
        params[tmparr[0]] = tmparr[1];
    }
    return params;
}

function fillInput(key, params) {
    if (params[key] != undefined) {
        $('#' + key).val(params[key]);
    }
}