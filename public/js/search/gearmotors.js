$(document).ready(function () {

    $('input.gearmotors').change(function () {
        getCountQuery();
    });
    $('select.gearmotors').change(function () {
        getCountQuery();
    });

    $(".number-only").keydown(function (e) {
        var input = $(this);
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode == 65 && e.ctrlKey === true) ||
            (e.keyCode == 67 && e.ctrlKey === true) ||
            (e.keyCode == 88 && e.ctrlKey === true) ||
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
            input.addClass('invalid');
        }
        if ($.isNumeric(input.val())) {
            input.removeClass('invalid');
        }
    });
});
function getDataGearMotors() {
    var data = {};
    data.id_equipment = "1";
    if ($('#select-group').val()) {
        data.id_group = $('#select-group').val();
    }
    if ($('#select-type').val()) {
        data.id_type = $('#select-type').val();
    }
    if ($('#speed_shaft_min').val()) {
        data.speed_shaft_min = $('#speed_shaft_min').val();
    }
    if ($('#speed_shaft_max').val()) {
        data.speed_shaft_max = $('#speed_shaft_max').val();
    }
    if ($('#torque_min').val()) {
        data.torque_min = $('#torque_min').val();
    }
    if ($('#torque_max').val()) {
        data.torque_max = $('#torque_max').val();
    }
    if ($('#service_factor_min').val()) {
        data.service_factor_min = $('#service_factor_min').val();
    }
    if ($('#service_factor_max').val()) {
        data.service_factor_max = $('#service_factor_max').val();
    }
    if ($('#gear_ratio_min').val()) {
        data.gear_ratio_min = $('#gear_ratio_min').val();
    }
    if ($('#gear_ratio_max').val()) {
        data.gear_ratio_max = $('#gear_ratio_max').val();
    }
    if ($('#motor_power').val()) {
        data.motor_power = $('#motor_power').val();
    }
    if ($('#number_poles').val()) {
        data.number_poles = $('#number_poles').val();
    }
    return data;
}

function autoFillGearmotors(params) {
    fillInput('speed_shaft_min', params);
    fillInput('speed_shaft_max', params);
    fillInput('torque_min', params);
    fillInput('torque_max', params);
    fillInput('service_factor_min', params);
    fillInput('service_factor_max', params);
    fillInput('gear_ratio_min', params);
    fillInput('gear_ratio_max', params);
    fillInput('motor_power', params);
    fillInput('number_poles', params);
}
