$(document).ready(function () {
    var params = getSearchParameters();
    var id_equipment = params['id_equipment'];
    var id_group = params['id_group'];
    var id_type = params['id_type'];

    $("#select-equipment").val(id_equipment);

    $.each(groups, function (key, value) {
        if (value.id_e == id_equipment) {
            addOptionInSelect("select-group", value)
        }
    });
    $("#select-group").val(id_group);

    $.each(types, function (key, value) {
        if (value.id_g == id_group) {
            addOptionInSelect("select-type", value)
        }
    });
    $("#select-type").val(id_type).trigger('change');

    if (id_equipment == 1) {
        autoFillGearmotors(params);
        getCountQuery();
    }
});

function getSearchParameters() {
    var prmstr = window.location.search.substr(1);
    return prmstr != null && prmstr != "" ? transformToAssocArray(prmstr) : {};
}

function transformToAssocArray(prmstr) {
    var params = {};
    var prmarr = prmstr.split("&");
    for (var i = 0; i < prmarr.length; i++) {
        var tmparr = prmarr[i].split("=");
        params[tmparr[0]] = tmparr[1];
    }
    return params;
}

function fillInput(key, params) {
    if (params[key] != undefined) {
        $('#' + key).val(params[key]);
    }
}