<!DOCTYPE html>
<html lang="ru">
<head>
	<title>
		<?php echo $data['card']['name']['value'] ?>
	</title>
	<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
	<link rel='stylesheet' href='/css/main.css'>
	<link rel='stylesheet' href='/css/card.css'>
	<script type="text/javascript" src="/js/materialize.js"></script>
	<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="/css/materialize.css" media="screen,projection"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

</head>
<body>
<div id="content">
	<div id="image-container">
		<h6 class="title"><?php echo $data['card']['name']['value'] ?></h6>
		<img class="preview" src="/img/example.jpg"/>
	</div>

	<table class="card-item highlight">
		<tbody>
		<?php
		foreach ($data["card"] as $card) {
			if (isset($card['value']) && $card['name'] != "Наименование") {
				?>
				<tr>
					<td class="column-name"><?php echo $card['name'] ?></td>
					<td class="column-value"><?php echo $card['value'] ?></td>
				</tr>
			<?php }
		}
		?>
		</tbody>
	</table>
</div>
</body>
</html>