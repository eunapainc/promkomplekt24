<!DOCTYPE html>
<html lang="ru">
<head>
	<title>
		Список
	</title>
	<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="/js/materialize.js"></script>
	<script type="text/javascript" src="/js/catalog-filter.js"></script>
	<script type="text/javascript" src="/js/search/gearmotors.js"></script>
	<script>
		var idType = <?php echo $data['id_type']?>;
		$(document).ready(function () {
			$('select').material_select();
		});
	</script>

	<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
	<link type="text/css" rel="stylesheet" href="/css/materialize.css" media="screen,projection"/>
	<link rel='stylesheet' href='/css/main.css'>
	<link rel='stylesheet' href='/css/list.css'>
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

</head>
<body>
<div id="content">
	<?php if (count($data["items"]) > 0) { ?>
	<div id="list">
		<table id="list" class="striped centered">
			<thead>
			<tr>
				<th>n2</th>
				<th>M2</th>
				<th>Sf</th>
				<th>i</th>
				<th>Обозначение</th>
			</tr>
			</thead>
			<tbody>
			<?php
			foreach ($data["items"] as $item) {
				$slug = $item['slug'];
				$output_shaft_speed = $item['output_shaft_speed'];
				$torque = $item['torque'];
				$service_factor = $item['service_factor'];
				$gear_ratio = $item['gear_ratio'];
				$name = $item['name'];
				?>
				<tr>
					<td><?php echo $output_shaft_speed ?></td>
					<td><?php echo $torque ?></td>
					<td><?php echo $service_factor ?></td>
					<td><?php echo $gear_ratio ?></td>
					<td><?php echo "<a href='$slug'>$name</a>" ?></td>
				</tr>
			<?php }
			?>
			</tbody>
		</table>
		<?php } else { ?>
			Нечего показывать
		<?php } ?>
		<div id="pagination" class="paginate wrapper">
			<ul class="pagination">
				<?php
				$page = $data['page'];
				$pagesCount = $data['pagesCount'];
				$baseUrl = $data['baseUrl'];

				$firstPageUrl = $baseUrl . "page=1";
				$lastPageUrl = $baseUrl . "page=$pagesCount";

				/** Show arrow-link to first page */
				if ($page > 1) {
					echo "<li class='waves-effect'><a href='$firstPageUrl'><i class='material-icons'>chevron_left</i></a></li>";
				}

				/** Show first pages links. All before current if current in first 5, else - 2. */
				$firstPageCount = $page <= 5 ? $page : 3;
				for ($i = 1; $i < $firstPageCount; $i++) {
					showPageLink($i, $baseUrl);
				}

				/** If current not in first 5, than show '...' and previous 2. */
				if ($page > 5) {
					echo "<span>...</span>";
					for ($i = $page - 2; $i < $page; $i++) {
						showPageLink($i, $baseUrl);
					}
				}

				echo "<li class='active'><a href='#'>$page</a></li>";

				/** If current not in last 5, than show '...' and next 2. */
				if ($page <= $pagesCount - 5) {
					for ($i = $page + 1; $i < $page + 3; $i++) {
						showPageLink($i, $baseUrl);
					}
					echo "<span>...</span>";
				}

				/** Show last pages links. All after current if current in last 5, else - 2. */
				$lastBlockStart = $page > $pagesCount - 5 ? $page + 1 : $pagesCount - 1;
				for ($i = $lastBlockStart; $i <= $pagesCount; $i++) {
					showPageLink($i, $baseUrl);
				}

				function showPageLink($target, $baseUrl)
				{
					$pageUrl = $baseUrl . "page=" . ($target);
					echo "<li class='waves-effect'><a href='$pageUrl'>$target</a></li>";
				}

				if ($page < $pagesCount) {
					echo "<li class='waves-effect'><a href='$lastPageUrl'><i class='material-icons'>chevron_right</i></a></li>";
				}
				?>
			</ul>
		</div>
	</div>
	<div id="gearmotors-search-form" class="advanced-search">
		<h6>Фильтр</h6>
		<div class="input-field col s5">
			<label>Скорость выходного вала (n2)</label>
			<input placeholder="от" id="speed_shaft_min" type="text"
			       class="validate short-form gearmotors number-only">
			<input placeholder="до" id="speed_shaft_max" type="text"
			       class="validate short-form gearmotors number-only">
		</div>
		<div class="input-field col s5">
			<label>Крутящий момент мотор-редуктора (M2)</label>
			<input placeholder="от" id="torque_min" type="text" class="validate short-form gearmotors number-only">
			<input placeholder="до" id="torque_max" type="text" class="validate short-form gearmotors number-only">
		</div>
		<div class="input-field col s5">
			<label>Сервис фактор (Sf)</label>
			<input placeholder="от" id="service_factor_min" type="text"
			       class="validate short-form gearmotors number-only">
			<input placeholder="до" id="service_factor_max" type="text"
			       class="validate short-form gearmotors number-only">
		</div>
		<div class="input-field col s5">
			<label>Предаточное число (i)</label>
			<input placeholder="от" id="gear_ratio_min" type="text"
			       class="validate short-form gearmotors number-only">
			<input placeholder="до" id="gear_ratio_max" type="text"
			       class="validate short-form gearmotors number-only">
		</div>
		<div class="input-field col s5">
			<select id="motor_power" class="gearmotors">
				<option selected disabled value="">Выберите мощность</option>
				<option value="0.04">0.04</option>
				<option value="0.06">0.06</option>
				<option value="0.09">0.09</option>
				<option value="0.12">0.12</option>
				<option value="0.18">0.18</option>
				<option value="0.25">0.25</option>
				<option value="0.3">0.3</option>
				<option value="0.37">0.37</option>
				<option value="0.55">0.55</option>
				<option value="0.75">0.75</option>
				<option value="0.9">0.9</option>
				<option value="1.1">1.1</option>
				<option value="1.5">1.5</option>
				<option value="1.84">1.84</option>
				<option value="1.85">1.85</option>
				<option value="2.2">2.2</option>
				<option value="3">3</option>
				<option value="3.3">3.3</option>
				<option value="4">4</option>
				<option value="5.5">5.5</option>
				<option value="6.3">6.3</option>
				<option value="7.5">7.5</option>
				<option value="9">9</option>
				<option value="9.2">9.2</option>
				<option value="10">10</option>
				<option value="11">11</option>
			</select>
		</div>
		<div class="input-field col s5">
			<select id="number_poles" class="gearmotors">
				<option selected disabled value="">Выберите количество полюсов</option>
				<option value="2">2</option>
				<option value="4">4</option>
				<option value="6">6</option>
			</select>
		</div>
		<div id="result"></div>
		<div id="loader"><img src="/img/loader.gif" alt="loader"></div>
	</div>

</div>
</body>
</html>