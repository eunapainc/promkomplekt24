<!DOCTYPE html>
<html lang="ru">
<head>
	<title>
		Типы
	</title>
	<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="/js/materialize.js"></script>
	<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
	<link rel='stylesheet' href='/css/main.css'>
	<link rel='stylesheet' href='/css/catalog.css'>
	<!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="/css/materialize.css" media="screen,projection"/>

	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

</head>
<body>
<div id="content">
	<div id="catalog">
		<?php
		$items = $data['items'];
		foreach ($items as $item) {
			?>

			<div class="catalog-item">
				<a href="<?php echo $item['slug']; ?>">
					<img src="/img/privoda.png">
					<p>
						<span><?php echo $item['name']; ?></span>
					</p>
				</a>
			</div>
			<?php
		}
		?>

	</div>
</div>
</body>
</html>