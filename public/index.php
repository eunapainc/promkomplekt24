<?php

use App\App;

include_once(dirname(__DIR__) . '/config.php');

spl_autoload_register(function ($class) {
	$parts = explode('\\', $class);
	$parts = array_slice($parts, 1);

	$path = dirname(__DIR__) . "/application/" . implode("/", $parts) . ".php";
	include_once $path;
});

define("PUBLIC_FOLDER", __DIR__);

$app = new App();
$app->init();